<?php

use Illuminate\Database\Seeder;

class MeasureUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mesureingUnitsArray = ['g','kg','pieces','litre'];
        foreach($mesureingUnitsArray as $mesureingUnit){
            factory('App\MeasureUnit')->create(['name' => $mesureingUnit]);
        }
    }
}
