<?php

use Illuminate\Database\Seeder;

class BoxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $boxes = factory('App\Box',4)->create()->each(function ($box){
            $recipe =factory(App\Recipe::class)->create(); 
            $recipe->ingredients()->sync([[
                'ingredient_id' => factory(App\Ingredient::class)->create()->id,
                'recipe_id' => $recipe->id,
                'ingredient_amount' => rand(1,1000)
            ],
            [
                'ingredient_id' => factory(App\Ingredient::class)->create()->id,
                'recipe_id' => $recipe->id,
                'ingredient_amount' => rand(1,1000)
            ]
            ]);
            $box->recipes()->sync([[
                'box_id' => $box->id ,
                'recipe_id' => $recipe->id
            ],
            [
                'box_id' => $box->id ,
                'recipe_id' => $recipe->id
            ],
            [
                'box_id' => $box->id ,
                'recipe_id' => $recipe->id
            ],
            [
                'box_id' => $box->id ,
                'recipe_id' => $recipe->id
            ]
            ]);
        });
    }
}
