<?php

use Illuminate\Database\Seeder;

class RecipeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $recipes = factory('App\Recipe',4)->create()->each(function ($recipe){
            $recipe->ingredients()->sync([[
                'ingredient_id' => factory(App\Ingredient::class)->create()->id,
                'recipe_id' => $recipe->id,
                'ingredient_amount' => rand(1,1000)
            ],
            [
                'ingredient_id' => factory(App\Ingredient::class)->create()->id,
                'recipe_id' => $recipe->id,
                'ingredient_amount' => rand(1,1000)
            ]
            ]);
        });
    }
}
