<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Ingredient;
use Faker\Generator as Faker;

$factory->define(Ingredient::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'measure_unit_id' => function(){
            return factory(App\MeasureUnit::class)->create()->id;
        },
        'supplier_id' => function(){
            return factory(App\Supplier::class)->create()->id;
        }
,
    ];
});
