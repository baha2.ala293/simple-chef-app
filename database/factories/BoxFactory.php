<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Box;
use App\User;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Box::class, function (Faker $faker) {
    return [
        'user_id' => factory(User::class)->create()->id,
        'delivery_date' => Carbon::now()->addHours(48)->toDateString()
    ];
});
