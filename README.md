** Simple Chef App**

# Approach

I created Simple Chef App using laravel verion 6 LTS.

I focused on completing the task by writing a clean architecture. I have extended laravel Error Handler and added custom error responses theirs for all Apis endpoints. I followed the Template method design pattern for creating transformers for all API responses so it will be consistent all over the app. I also created a Trait responsible for formatting all JSON responses. Finally, I added some tests to make sure everything works smoothly on production before deployment.

Note: I didn't write all possible test cases for the app.

# Assumptions


[Composer Installed](https://getcomposer.org/download/)
[Docker Installed](https://www.docker.com/)

## Installation


* cd to your server directory and pull the project : https://gitlab.com/baha2.ala293/simple-chef-app
* cd to the project directory and run  from bash window> bash start.sh  

## API Documentation
*[Documentation](https://docs.google.com/document/d/1oAnImb5iEw0A-lXw87UV8XAIRNrnaiHzZ7Qa_W4i6aY/edit?usp=sharing)

## Run tests
 > vendor/bin/phpunit



