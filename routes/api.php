<?php


Route::group(['prefix' => 'v1', 'namespace' => 'Api\V1'], function () {

    /**Ingredients Routes */
    Route::get('ingredients'            , 'IngredientController@index');
    Route::post('ingredients'           , 'IngredientController@store');
    Route::get('ingredients/for/orders' , 'IngredientController@warehouseNeeds');

    /**Recipes Routes */
    Route::get('recipes'    , 'RecipeController@index');
    Route::post('recipes'   , 'RecipeController@store');

    /**Boxes Routes */
    Route::post('boxes'   , 'BoxController@store');



});
