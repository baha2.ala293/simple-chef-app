<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    protected $fillable = ['id','name','supplier_id','measure_unit_id'];


    public function supplier()
    {
        return $this->hasOne('App\Supplier','id','supplier_id')->withDefault([
            'name' => 'N/A',
        ]);
    }

    public function measure()
    {
        return $this->hasOne('App\MeasureUnit','id','measure_unit_id')->withDefault([
            'name' => 'N/A',
        ]);
    }

    public function recipes()
    {
        return $this->belongsToMany('App\Recipe','recipe_ingredients','ingredient_id', 'recipe_id');
    }
}
