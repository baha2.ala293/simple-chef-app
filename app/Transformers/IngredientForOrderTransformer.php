<?php

namespace App\Transformers;
use App\Transformers\BaseTransformer as Transformer;


class IngredientForOrderTransformer extends Transformer
{

    public function transform( $ingredient ) : array
    {
        return[
            'id'            => $ingredient->id,
            'total_amount'  => $ingredient->total_amount,
            'name'          => $ingredient->name,
            'supplier'      => $ingredient->supplier,
            'measure'       => $ingredient->measure
        ];
    }

}