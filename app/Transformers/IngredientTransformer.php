<?php

namespace App\Transformers;
use App\Transformers\BaseTransformer as Transformer;


class IngredientTransformer extends Transformer
{

    public function transform( $ingredient ) : array
    {
        return[
            'id'        => $ingredient->id,
            'name'      => $ingredient->name,
            'supplier'  => $ingredient->supplier->name,
            'measure'   => $ingredient->measure->name
        ];
    }

}