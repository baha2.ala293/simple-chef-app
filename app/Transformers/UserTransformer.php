<?php

namespace App\Transformers;
use App\Transformers\BaseTransformer as Transformer;


class UserTransformer extends Transformer
{

    public function transform( $user ) : array
    {
        return[
            'id'        => $user->id,
            'name'      => $user->name,
            'email'     => $user->email
        ];
    }

}