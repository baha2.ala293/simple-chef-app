<?php

namespace App\Transformers;
use App\Transformers\RecipeIngredientTransformer;
use App\Transformers\BaseTransformer as Transformer;


class RecipeTransformer extends Transformer
{

    protected $recipeIngredientTransformer;

    function __construct(RecipeIngredientTransformer $recipeIngredientTransformer)
    {
        $this->recipeIngredientTransformer = $recipeIngredientTransformer;
    }

    public function transform( $recipe ) : array
    {
        return[
            'id'            => $recipe->id,
            'name'          => $recipe->name,
            'description'   => $recipe->description,
            'ingredients'   => $this->recipeIngredientTransformer->transformCollection($recipe->ingredients)
        ];
    }

}