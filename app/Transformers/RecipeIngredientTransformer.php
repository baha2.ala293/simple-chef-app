<?php

namespace App\Transformers;
use App\Transformers\BaseTransformer as Transformer;


class RecipeIngredientTransformer extends Transformer
{

    public function transform( $ingredient ) : array
    {
        return[
            'id'        => $ingredient->id,
            'name'      => $ingredient->name,
            'amount'    => $ingredient->pivot->ingredient_amount,
            'measure'   => $ingredient->measure->name
        ];
    }

}