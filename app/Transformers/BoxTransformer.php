<?php

namespace App\Transformers;
use App\Transformers\RecipeTransformer;
use App\Transformers\BaseTransformer as Transformer;


class BoxTransformer extends Transformer
{
    protected $recipeTransformer;
    protected $userTransformer;

    function __construct(RecipeTransformer $recipeTransformer, UserTransformer $userTransformer)
    {
        $this->recipeTransformer = $recipeTransformer;
        $this->userTransformer = $userTransformer;
    }

    public function transform( $box ) : array
    {
        return[
            'id'               => $box->id,
            'user'             => $this->userTransformer->transform($box->user),
            'delivery_date'    => $box->delivery_date,
            'recipes'          => $this->recipeTransformer->transformCollection($box->recipes)
        ];
    }

}