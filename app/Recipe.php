<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    protected $fillable = ['name','description'];

    public function ingredients()
    {
        return $this->belongsToMany('App\Ingredient','recipe_ingredients','recipe_id','ingredient_id')
                    ->withPivot('ingredient_amount');
    }

    public function boxes()
    {
        return $this->belongsToMany('App\Box','box_recipe','recipe_id','box_id');
    }
}
