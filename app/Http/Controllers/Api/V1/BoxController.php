<?php

namespace App\Http\Controllers\Api\V1;

use App\Box;
use Carbon\Carbon;
use App\Traits\Restable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Transformers\BoxTransformer;

class BoxController extends Controller
{
    use Restable;

    protected $boxTransformer;

    function __construct(BoxTransformer $boxTransformer)
    {
        $this->boxTransformer = $boxTransformer;
    }

    public function store(Request $request)
    {
        $after48HoursDate = Carbon::now()->addHours(48)->toDateString();

        $rules = [
            'user_id'       => 'required|numeric|exists:users,id',
            'delivery_date' => 'required|date|date_format:Y-m-d|after:'.$after48HoursDate,
            'recipes'       => 'required|array|between:1,4',
            'recipes.*'     => 'required|numeric|exists:recipes,id'
        ];

        $this->validate($request, $rules);

        $box = Box::create($request->only('user_id','delivery_date'));

        $box->recipes()->sync($request->recipes);

        return $this->setStatusCode(Response::HTTP_CREATED)->respond($this->boxTransformer->transform($box));
    }
}
