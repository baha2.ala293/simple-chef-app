<?php

namespace App\Http\Controllers\Api\V1;

use App\Recipe;
use App\Traits\Restable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Transformers\RecipeTransformer;

class RecipeController extends Controller
{
    use Restable;

    protected $recipeTransformer;

    function __construct(RecipeTransformer $recipeTransformer)
    {
        $this->recipeTransformer = $recipeTransformer;
    }


    public function index(Request $request){

        if ($request->limit) {
            $this->setPagination($request->limit);
        }

        $pagination = Recipe::paginate($this->getPagination());
        $recipes    = $this->recipeTransformer->transformCollection(collect($pagination->items()));
        return $this->respondWithPagination($pagination,['data' => $recipes]);

    }

    public function store(Request $request)
    {
        $rules = [
            'name'                              => 'required|string|min:1|max:255',
            'description'                       => 'required|string|min:1|max:500',
            'ingredients'                       => 'required|array',
            'ingredients.*.ingredient_id'       => 'required|numeric|exists:ingredients,id',
            'ingredients.*.ingredient_amount'   => 'required|numeric'
        ];

        $this->validate($request, $rules);

        $recipe = Recipe::create($request->only('name','description'));
        $recipe->ingredients()->sync($request->ingredients);
        
        return $this->setStatusCode(Response::HTTP_CREATED)->respond($this->recipeTransformer->transform($recipe));
    }
}
