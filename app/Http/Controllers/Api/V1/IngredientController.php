<?php

namespace App\Http\Controllers\Api\V1;

use App\Box;
use Carbon\Carbon;
use App\Ingredient;
use App\Traits\Restable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Transformers\IngredientTransformer;
use App\Transformers\IngredientForOrderTransformer;

class IngredientController extends Controller
{
    use Restable;

    
    protected $ingredientTransformer;
    protected $ingredientForOrderTransformer;

    function __construct(IngredientTransformer $ingredientTransformer,
                                                IngredientForOrderTransformer $ingredientForOrderTransformer)
    {
        $this->ingredientTransformer = $ingredientTransformer;
        $this->ingredientForOrderTransformer = $ingredientForOrderTransformer;
    }


    public function index(Request $request)
    {
        if ($request->limit) {
            $this->setPagination($request->limit);
        }

        $pagination = Ingredient::paginate($this->getPagination());
        $ingredients  = $this->ingredientTransformer->transformCollection(collect($pagination->items()));
        return $this->respondWithPagination($pagination,['data' => $ingredients]);
    }


    public function store(Request $request)
    {
        
        $rules = [
            'name'              => 'required|string|min:1|max:255',
            'supplier_id'       => 'required|numeric|exists:suppliers,id',
            'measure_unit_id'   => 'required|numeric|exists:measure_units,id'
        ];

        $this->validate($request, $rules);

        $ingredient = $this->ingredientTransformer->transform(Ingredient::create($request->all()));

        return $this->setStatusCode(Response::HTTP_CREATED)->respond(['data' => $ingredient]);

    }


    public function warehouseNeeds(Request $request)
    {
        $rules = [
            'order_date'    =>  'required|date|date_format:Y-m-d',
            'supplier_id'   =>  'nullable|numeric|exists:suppliers,id',
            'delivery_date' =>  'nullable|numeric|date_format:Y-m-d'
        ];

        $this->validate($request, $rules);

        $after7Days = Carbon::parse($request->order_date)->addDays(7)->toDateString();
    
        $ingredients = (new Box)->newQuery();

        $ingredients = $ingredients->whereBetween('delivery_date',[$request->order_date,$after7Days])
                                ->join('box_recipes','id','=','box_recipes.box_id')
                                ->join('recipes','recipes.id','=','box_recipes.recipe_id')
                                ->join('recipe_ingredients', 'recipes.id','=','recipe_ingredients.recipe_id')
                                ->join('ingredients','ingredients.id','=','recipe_ingredients.ingredient_id')
                                ->join('suppliers','ingredients.supplier_id','=','suppliers.id')
                                ->join('measure_units' ,'ingredients.measure_unit_id','=','measure_units.id')
                                ->select(DB::raw('ingredients.id as id ,sum(ingredient_amount) as total_amount,
                                 ingredients.name, suppliers.name as supplier, measure_units.name as measure'));
        if($request->has('supplier_id')){
            $ingredients->where('suppliers.id',$request->supplier_id);
        }
        if($request->has('delivery_date')){
            $ingredients->whereDate('boxes.delivery_date',$request->delivery_date);
        }
        $result =  $ingredients->groupBy('ingredients.id')
                                ->get();

        return $this->respond(['data' => $this->ingredientForOrderTransformer->transformCollection($result)]);
        
    }
}
