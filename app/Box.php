<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    protected $fillable = ['user_id','delivery_date'];

    protected $with = ['recipes'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function recipes()
    {
        return $this->belongsToMany('App\Recipe','box_recipes','box_id','recipe_id');
    }
}
