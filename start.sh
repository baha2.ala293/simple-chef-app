#!/bin/bash
echo "*** Starting Simple Chef App Initialization ***"
cp .env.example .env
docker-compose up -d --build
docker-compose exec app sh tests.sh