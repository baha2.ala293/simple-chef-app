#!/bin/bash
line=$(head -n 1 /tmp/db_done 2>/dev/null)
until [ "$line" = "1" ]
do
    line=$(head -n 1 /tmp/db_done 2>/dev/null)
    echo "Waiting for database to initialize...."
    sleep 5
done
vendor/bin/phpunit