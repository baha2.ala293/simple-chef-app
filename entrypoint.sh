#!/bin/bash
until nc -z -v -w30 $DB_HOST 3306
do
  echo "Waiting for database connection..."
  sleep 5
done
echo "0" > /tmp/db_done
composer install
php artisan key:generate
php artisan config:clear
php artisan config:cache
php artisan migrate:fresh
php artisan migrate --database=sqlite
php artisan db:seed
composer dump-autoload
echo "1" > /tmp/db_done
php-fpm