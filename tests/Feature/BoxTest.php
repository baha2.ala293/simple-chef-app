<?php

namespace Tests\Feature;

use App\Box;
use App\User;
use App\Recipe;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BoxTest extends TestCase
{
    use WithFaker, RefreshDatabase;


    /** @test */
    public function create_box_required_valied_recipes_ids()
    {
        
        $user = factory(User::class)->make();

        $payload = [
            "user_id" => $user->id,
	        "delivery_date"  => Carbon::now()->addHours(48)->toDateString(),
	        "recipes" => [100,10022]
            ];

        $this->json('POST', 'api/v1/boxes', $payload )
        ->assertStatus(422)
        ->assertSee("The selected recipes.0 is invalid.");
        
    }



     /** @test */
     public function create_box_delivery_date_must_be_with_in_48_hours()
     {
         
         $user = factory(User::class)->make();
         $recipe = factory(Recipe::class)->make();
 
         $payload = [
             "user_id" => $user->id,
             "delivery_date"  => Carbon::now()->toDateString(),
             "recipes" => [$recipe->id]
             ];
 
         $this->json('POST', 'api/v1/boxes', $payload )
         ->assertStatus(422)
         ->assertSee("The delivery date must be a date after ".Carbon::now()->addHours(48)->toDateString());
     }
}
