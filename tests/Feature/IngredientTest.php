<?php

namespace Tests\Feature;

use App\Ingredient;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class IngredientTest extends TestCase
{

    use WithFaker, RefreshDatabase;
    

    /** @test */
    public function can_create_ingredient()
    {
        
        $ingredient = factory(Ingredient::class)->make();

        $payload = ['name' => $ingredient->name,
                    'measure_unit_id' => $ingredient->measure_unit_id,
                    'supplier_id' => $ingredient->supplier_id
                ];

        $this->json('POST', 'api/v1/ingredients', $payload )
        ->assertStatus(201)
        ->assertSee($ingredient->name)
        ->assertSee($ingredient->supplier->name)
        ->assertSee($ingredient->measure->name)
        ->assertJsonStructure([
            "data" => [
                "id",
                "name",
                "supplier",
                "measure"
        ]]);
    }

    /** @test */
    public function can_view_all_ingredients_with_pagination()
    {
        $this->get('api/v1/ingredients')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data'=>[
                '*' => [
                    'id',
                    'name',
                    'supplier',
                    'measure',
                ]
                ],
                 "pagination"=> [
                        "total_count",
                        "total_pages",
                        "current_page",
                        "limit",
                        "next_page_url",
                        "prev_page_url"
                    ]
            ]);
    }



}
