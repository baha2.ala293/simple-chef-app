<?php

namespace Tests\Feature;

use App\Recipe;
use App\Ingredient;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RecipeTest extends TestCase
{
    use WithFaker, RefreshDatabase;
    

    /** @test */
    public function creating_recipe_required_ingredient_and_amount_for_it()
    {
        
        $recipe = factory(Recipe::class)->make();
        $ingredient = factory(Ingredient::class)->make();

        $payload = ['name' => $ingredient->name,
                    'description' => $ingredient->description,
                    'ingredients' => [
                        ["ingredient_id" => $ingredient->id ]
                    ]
                ];

        $this->json('POST', 'api/v1/recipes', $payload )
        ->assertStatus(422)
        ->assertJson([
                "error" =>  [
                    "message" => [
                        "ingredients.0.ingredient_amount"=> [
                            "The ingredients.0.ingredient_amount field is required."
                        ]
                    ],
                    "status_code" =>422
            ]
        ]);
        
    }


    /** @test */
    public function can_view_all_recipes_with_pagination()
    {
        $this->get('api/v1/recipes')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data'=>[
                '*' => [
                    'id',
                    'name',
                    'description',
                    'ingredients' => [
                        '*' => [
                            "id",
                            "name",
                            "amount",
                            "measure"
                        ]
                    ]
                ]
                ],
                 "pagination"=> [
                        "total_count",
                        "total_pages",
                        "current_page",
                        "limit",
                        "next_page_url",
                        "prev_page_url"
                    ]
            ]);
    }
}
